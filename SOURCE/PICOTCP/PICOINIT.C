/*
 * init helper for picotcp4dos
 * Copyright (C) 2015 Mateusz Viste
 *
 * http://picotcp4dos.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>  /* getenv() */
#include <string.h>  /* memset() */

#include "picotcp.h"
#include "pktdrv.h"
#include "ipcfg\picoconf.h"
#include "version.h"

#include "picodos.h"  /* include self for control */



/* a no-op function, used as a destroy() callback so picoTCP is happy -- the
 * documentation is unclear on whether this is required or not */
static void far fakefunc(struct pico_device *device) {
  /* nothing to do */
}


static int isipv6(unsigned char *ip) {
  uint16_t *ptr = (uint16_t *)ip;
  if ((ptr[0] == 0) && (ptr[1] == 0) && (ptr[2] == 0) && (ptr[3] == 0) && (ptr[4] == 0) && (ptr[5] == 0xffffu)) return(0);
  return(1);
}


static void ip2picoip4(unsigned char *ip, struct pico_ip4 *picoip) {
  int x;
  picoip->addr = 0;
  for (x = 15; x >= 12; x--) {
    picoip->addr <<= 8;
    picoip->addr |= ip[x];
  }
}


static void masklen2picoip4(int masklen, struct pico_ip4 *picoip) {
  picoip->addr = 0;
  picoip->addr = ~picoip->addr;
  masklen = 32 - masklen;
  picoip->addr >>= masklen;
}


struct buff_struct {
  unsigned char ip1[16];
  unsigned char ip2[16];
  unsigned char mymac[6];
};

/* configures the picodev device with IP, DNS, gateway */
static int picoinit_ipconf(FILE *fd, struct buff_struct *buff, struct pico_device *picodev) {
  int count, tmpint;
  /* configure IP addresses */
  count = picoconf_getip(fd, NULL, NULL);
  while (count-- > 0) {
    picoconf_getip(fd, buff->ip1, &tmpint);
    if (isipv6(buff->ip1) != 0) {
      /* IPv6 TODO */
    } else { /* IPv4 */
      struct pico_ip4 picoip, netmask;
      ip2picoip4(buff->ip1, &picoip);
      masklen2picoip4(tmpint, &netmask);
      if (pico_ipv4_link_add(picodev, picoip, netmask) != 0) return(PICOINIT_LINKADD);
    }
  }

  /* configure routes */
  count = picoconf_getrt(fd, NULL, NULL, NULL);
  while (count-- > 0) {
    picoconf_getrt(fd, buff->ip1, &tmpint, buff->ip2);
    /* printf("%d.%d.%d.%d/%d -> %d.%d.%d.%d\n", buff->ip1[12], buff->ip1[13], buff->ip1[14], buff->ip1[15], tmpint, buff->ip2[12], buff->ip2[13], buff->ip2[14], buff->ip2[15]); */
    if (isipv6(buff->ip1) != 0) {
      /* IPv6 TODO */
    } else { /* IPv4 */
      struct pico_ip4 picoip, picogw, netmask;
      ip2picoip4(buff->ip1, &picoip);
      ip2picoip4(buff->ip2, &picogw);
      masklen2picoip4(tmpint, &netmask);
      if (pico_ipv4_route_add(picoip, netmask, picogw, 1, NULL) != 0) return(PICOINIT_ROUTEADD);
    }
  }

  /* init the DNS client module and configure DNS servers */
  if (pico_dns_client_init() != 0) return(PICOINIT_DNSINIT);
  count = picoconf_getdns(fd, NULL);
  while (count-- > 0) {
    picoconf_getdns(fd, buff->ip1);
    if (isipv6(buff->ip1) != 0) {
      /* IPv6 TODO */
    } else { /* IPv4 */
      struct pico_ip4 picoip;
      ip2picoip4(buff->ip1, &picoip);
      if (pico_dns_client_nameserver(&picoip, PICO_DNS_NS_ADD) != 0) return(PICOINIT_DNSADD);
    }
  }

  return(0);
}


int picoinit(struct pico_device *picodev, int flags) {
  FILE *fd;
  char *cfgfile;
  struct buff_struct *buff;
  int count;
  int tmpint;

  /* open config file */
  cfgfile = getenv("PICOTCP");
  fd = picoconf_open(cfgfile);
  if (fd == NULL) {
    return(PICOINIT_CONFIG);
  }

  /* allocate some memory */
  buff = malloc(sizeof(struct buff_struct));
  if (buff == NULL) {
    picoconf_close(fd);
    return(PICOINIT_NOMEM);
  }

  /* get pkt int and setup pkt drv */
  count = picoconf_getint(fd, NULL);
  picoconf_getint(fd, &tmpint);

  /* init the packet driver */
  if (pktdrv_init(tmpint) != 0) {
    free(buff);
    picoconf_close(fd);
    return(PICOINIT_PKTDRV);
  }
  pktdrv_getaddr(buff->mymac);
  pico_stack_init();

  /* init pico device - this should land in some pico_dos_create() later */
  memset(picodev, 0, sizeof(struct pico_device));
  if (pico_device_init(picodev, "eth0", buff->mymac) != 0) {
    free(buff);
    picoconf_close(fd);
    pktdrv_quit();
    return(PICOINIT_DEVICE);
  }
  picodev->overhead = 0;
  picodev->send = pktdrv_send;
  picodev->poll = pktdrv_poll;
  picodev->destroy = fakefunc;

  pico_stack_tick();

  if ((flags & PICOINIT_NOCONF) == 0) {
    tmpint = picoinit_ipconf(fd, buff, picodev);
    if (tmpint != 0) {
      free(buff);
      picoconf_close(fd);
      pktdrv_quit();
      return(tmpint);
    }
    pico_stack_tick();
  }

  /* cleanup mem and return success */
  free(buff);
  picoconf_close(fd);
  return(0);
}


char *picover(void) {
  return(PVER);
}


void picoquit(struct pico_device *picodev) {
  pktdrv_quit();
}
